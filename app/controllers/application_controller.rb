class ApplicationController < ActionController::API

  #executes a shell script and returns the output of the BASH shell script as JSON e.g. {"result":"123"}

  def uptime
    result = `uptime`
    render json: { result: result.chop }
  end

end




