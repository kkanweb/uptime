require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  describe 'GET #uptime' do
    it 'shows uptime correctly' do
      get :uptime
      expect(JSON.parse(response.body)['result']).to eq `uptime`.chop
    end
  end
end


